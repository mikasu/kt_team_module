<?php

namespace KTteam\HelloWorld\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tableName = $setup->getTable('hello_world_table');
        //check if the table is already exists
        if ($setup->getConnection()->isTableExists($tableName) == true){
            //declare data
            $data = [
                [
                    'name' => 'How to create a simple module',
                    'description' => 'The description',
                    'status' => 1
                ],
                [
                    'name' => 'Create a module with custom database table',
                    'description' => 'The description',
                    'status' => 1
                ]
            ];

            //insert data to table
            foreach ($data as $item){
                $setup->getConnection()->insert($tableName, $item);
            }
        }

        $setup->endSetup();
    }
}