<?php

namespace KTteam\HelloWorld\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface as InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface as ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface as SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        //get table
        $tableName = $installer->getTable('hello_world_table');

        //check if the table already exists
        if ($installer->getConnection()->isTableExists('hello_world_table') != true){
            //create the table
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false,
                        'default' => 'Empty Name'
                    ],
                    'Name'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false,
                        'default' => 'Empty Description'
                    ],
                    'Description'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false,
                        'default' => 'Empty Status'
                    ],
                    'Status'
                )
                ->setComment('Hello World module table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}