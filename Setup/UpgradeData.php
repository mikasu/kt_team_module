<?php


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{



    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();



        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            //get the table
            $tableName = $setup->getTable('hello_world_table');
            //check if the table already exist
            if ($setup->getConnection()->isTableExists($tableName) == true){


                //declare data
                $data = [
                    [
                        'name' => 'How to create a simple module',
                        'description' => 'The description',
                        'status' => 1
                    ],
                    [
                        'name' => 'Create a module with custom database table',
                        'description' => 'The description',
                        'status' => 1
                    ]
                ];

                //insert data to table
                foreach ($data as $item){
                    $setup->getConnection()->insert($tableName, $item);
            }
            }
        }

        $setup->endSetup();
    }
}