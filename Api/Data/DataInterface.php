<?php
namespace KTteam\HelloWorld\Api\Data;

/**
 * Interface DataInterface
 * @package KTteam\Api\Data
 * @api
 */
interface DataInterface
{
    /**#@+
     * Constants
     * @var string
     */
    const ID = 'id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const STATUS = 'status';
    /**#@-*/

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status);

}
