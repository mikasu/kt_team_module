<?php

namespace KTteam\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Action as Action;
use Magento\Framework\App\Action\Context as Context;
use Magento\Framework\View\Result\PageFactory as PageFactory;

class Index extends Action
{
    protected $_pageFactory;
    public function __construct(
        Context $context,
        PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        return $this->_pageFactory->create();

    }
}