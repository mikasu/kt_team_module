<?php

namespace KTteam\HelloWorld\Model;

use KTteam\HelloWorld\Api\Data\DataInterface;
use KTteam\HelloWorld\Model\ResourceModel\Data as DataResource;
use Magento\FrameWork\Model\AbstractModel;

class Data extends AbstractModel implements DataInterface
{

    /**
     * @var string
     */
    protected $_idFieldName = DataInterface::ID;

  protected function _construct()
  {
      $this->_init(DataResource::class);
  }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData(DataInterface::NAME);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->setData(DataInterface::NAME, $name);
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(DataInterface::DESCRIPTION);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->setData(DataInterface::DESCRIPTION, $description);
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(DataInterface::STATUS);
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status)
    {
        $this->setData(DataInterface::STATUS, $status);
        return $this;
    }
}