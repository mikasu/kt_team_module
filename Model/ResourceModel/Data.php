<?php

namespace KTteam\HelloWorld\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use KTteam\HelloWorld\Api\Data\DataInterface;
class Data extends AbstractDb
{
    const TABLE_NAME = 'hello_world_table';


    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, DataInterface::ID);
    }
}