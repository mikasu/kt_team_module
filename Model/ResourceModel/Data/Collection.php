<?php

namespace KTteam\HelloWorld\Model\ResourceModel\Data;

use KTteam\HelloWorld\Model\Data;
use KTteam\HelloWorld\Model\ResourceModel\Data as DataResource;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
           Data ::class,
           DataResource::class
        );
    }

}