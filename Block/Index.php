<?php

namespace KTteam\HelloWorld\Block;


use KTteam\HelloWorld\Model\ResourceModel\Data\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Index extends Template
{

    protected $test;

    public function __construct(Context $context,
                                CollectionFactory $test)
    {
        $this->test = $test;
        parent::__construct($context);
    }


    public function getDBCollection()
    {
        $table_collection = $this->test->create();
        return $table_collection;
    }

}

?>
